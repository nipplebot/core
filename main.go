package main

import (
  "f4b.io/nipplebot/core/cmd"
)

func init() {
  InitApp()
}

// InitApp initializes the core
func InitApp() {
  Log.Info("f4b.io/nipplebot/core: initialized!")
}

func main() {
  cmd.Execute()
}
