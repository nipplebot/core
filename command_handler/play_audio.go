package command_handler // replyPongToPing is a handler that replies pong to ping messages

import (
  "context"
  "f4b.io/nipplebot/core/globals"
  "fmt"
  "github.com/andersfylling/disgord"
  "os"
  "strings"
  "time"
)

func PlayAudio(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debugf("PlayAudio(session, data): '%+v' '%+v'", session, data)

  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()

  var params = strings.TrimSpace(data.Message.Content)
  var err error

  Log.Debugf("PlayAudio(params): '%+v'", params)

  if len(params) == 0 {
    _, err = data.Message.Reply(ctx, session, fmt.Sprintf("invalid usage, no idea wtf '%s' is", params))
    CheckErrorAndExit(err)

    _, err = data.Message.Reply(ctx, session, fmt.Sprintf("usage: !play <SoundName>"))
    CheckErrorAndExit(err)

    return
  }

  guild, err := session.GetGuild(ctx, data.Message.GuildID)
  CheckErrorAndExit(err)

  if VoiceConnections[guild.ID.String()] == nil {
    _, err = data.Message.Reply(ctx, session, "im not in any voice channel you are - come and get me with **!come**")
    CheckErrorAndExit(err)

    return
  }

  //soundFilePath := filepath.Join(globals.SoundsDirectory, fmt.Sprintf("%s.dca", params))
  soundFilePath := fmt.Sprintf("%s/%s.dca", globals.SoundsDirectory, params)

  if _, err := os.Stat(soundFilePath); err == nil {
    // soundFilePath exists
    sleepDuration, err := time.ParseDuration("500ms")
    CheckErrorAndExit(err)

    soundFile, err := os.Open(soundFilePath)
    CheckErrorAndExit(err)

    err = VoiceConnections[guild.ID.String()].StartSpeaking()
    CheckErrorAndExit(err)

    time.Sleep(sleepDuration)

    err = VoiceConnections[guild.ID.String()].SendDCA(soundFile)
    CheckErrorAndExit(err)

    time.Sleep(sleepDuration)
    err = VoiceConnections[guild.ID.String()].SendDCA(soundFile)
    CheckErrorAndExit(err)

    err = soundFile.Close()

  } else if os.IsNotExist(err) {
    // soundFilePath does *not* exist
    _, err := data.Message.Reply(ctx, session, fmt.Sprintf("unable to find file: '%s'", soundFilePath))
    CheckErrorAndExit(err)

  } else {
    // Schrodinger: file may or may not exist. See err for details.

    // Therefore, do *NOT* use !os.IsNotExist(err) to test for file existence
    Log.Errorf("Error with file: '%+v' '%+v'", params, err)
  }
}
