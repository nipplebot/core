package command_handler

import (
  "context"
  "fmt"
  "github.com/andersfylling/disgord"
  "time"
)

func Leave(session disgord.Session, data *disgord.MessageCreate, args []string) {
  Log.Debugf("Leave(session, data): '%+v' '%+v'", session, data)

  var err error
  var replyMsg string
  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()
  msg := data.Message

  guild, err := session.GetGuild(ctx, data.Message.GuildID)
  if err != nil {
    Log.Errorf("Leave(Error): session.GetGuild:", err)
    return
  }

  if VoiceConnections[guild.ID.String()] != nil {
    replyMsg = fmt.Sprintf("Leave: I am leaving voice channel in guild '%s'", guild.Name)

    err = VoiceConnections[guild.ID.String()].Close()
    if err != nil {
      Log.Errorf("Leave(Error):Close:", err)
      return
    }

    _, err := msg.Reply(ctx, session, replyMsg)
    if err != nil {
      Log.Errorf("Leave(Error): msg.Reply:", err)
      return
    }
  }
}
