package command_handler

import (
  "context"
  "github.com/andersfylling/disgord"
  "strings"
  "time"
)

func Help(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debugf("Help(session, data): '%+v' '%+v'", session, data)

  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()
  //serverID := channel.GuildID
  author := data.Message.Author
  var msg strings.Builder


  channel, err := session.GetChannel(ctx, data.Message.ChannelID)
  CheckErrorAndExit(err)

  msg.WriteString("**!play** <url> - play youtube video; queues up if another video is playing")
  msg.WriteString("\n")
  msg.WriteString("**!skip** - skip currently playing video")
  msg.WriteString("\n")
  msg.WriteString("**!stop** - stops Video and clears queue")
  msg.WriteString("\n")
  msg.WriteString("**!come [voice]** - join voice channel")
  msg.WriteString("\n")
  msg.WriteString("**!leave [voice]** - leave voice channel")
  msg.WriteString("\n")
  msg.WriteString("**!list** - get List of Sound Names")
  msg.WriteString("\n")
  msg.WriteString("**!<sound name>** - say <sound name>")

  Log.Debugf("List===> '%+v' '%+v'", author, channel)
  CheckErrorAndExit(err)
}
