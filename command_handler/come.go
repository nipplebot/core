package command_handler

import (
  "context"
  "fmt"
  "github.com/andersfylling/disgord"
  "time"
)

func Come(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debugf("Come(session, data): '%+v' '%+v'", session, data)

  var ctx context.Context
  var err error
  var replyMsg string
  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()
  author := data.Message.Author
  msg := data.Message

  guild, err := session.GetGuild(ctx, data.Message.GuildID)
  if err != nil {
    Log.Errorf("Come(Error): session.GetGuild:", err)
    return
  }

  if VoiceConnections[guild.ID.String()] != nil {
    replyMsg = fmt.Sprintf("Come: I am already in the same voice channel as user '%s' in guild '%s'", author.Username, guild.Name)

    _, err := msg.Reply(ctx, session, replyMsg)
    if err != nil {
      Log.Errorf("Come(Error): msg.Reply:", err)
      return
    }
    return
  }

  var voiceChannel *disgord.Channel
  Log.Debug("Come(VoiceStates):", guild.VoiceStates)

  for _, vs := range guild.VoiceStates {
    Log.Debug("Come(VoiceState):", vs)

    if vs.UserID == author.ID {
      voiceChannel, err = session.GetChannel(ctx, vs.ChannelID)
      if err != nil {
        Log.Errorf("Come(Error): session.GetChannel:", err)
        return
      }
    }
  }
  if voiceChannel == nil {
    replyMsg = fmt.Sprintf("Come: unable to find voice channel of user '%s' in guild '%s'", author.Username, guild.Name)

    _, err := msg.Reply(ctx, session, replyMsg)
    if err != nil {
      Log.Errorf("Come(Error): msg.Reply:", err)
      return
    }
    return
  }

  voiceConnection, err := session.VoiceConnect(guild.ID, voiceChannel.ID)
  if voiceConnection != nil {
    VoiceConnections[guild.ID.String()] = voiceConnection
  }
}
