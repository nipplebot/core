package command_handler

import (
  "context"
  "github.com/andersfylling/disgord"
  "time"
)

func Next(session disgord.Session, data *disgord.MessageCreate, args []string) {
  Log.Debugf("Next(session, data): '%+v' '%+v'", session, data)

  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()

  channel, err := session.GetChannel(ctx, data.Message.ChannelID)
	CheckErrorAndExit(err)

	if args[0] == "vpl" {
		// TODO: this;
	} else if args[0] == "create" {
		// TODO: this https://timee.io/i/dev
	}

	message := disgord.CreateMessageParams{Content: "not implemented yet"}
	_, err = session.CreateMessage(ctx, channel.ID, &message)
	CheckErrorAndExit(err)
}
