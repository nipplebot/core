package command_handler // replyPongToPing is a handler that replies pong to ping messages

import (
  "context"
  "github.com/andersfylling/disgord"
  "time"
)

func Stop(session disgord.Session, data *disgord.MessageCreate) {
	Log.Debugf("Stop(session, data): '%+v' '%+v'", session, data)

  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()

	msg := data.Message

	// whenever the message written is "ping", the bot replies "pong"
	if msg.Content == "ping" {
		_, err := msg.Reply(ctx, session, "pong")
		CheckErrorAndExit(err)
	}
}
