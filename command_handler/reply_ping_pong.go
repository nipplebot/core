package command_handler // replyPongToPing is a handler that replies pong to ping messages

import (
  "context"
  "github.com/andersfylling/disgord"
  "regexp"
  "time"
)

func getChannel(evt interface{}) (msg *disgord.Channel) {
  channel := evt.(*disgord.Channel)
  return channel
}

func getMsg(evt interface{}) (msg *disgord.Message) {
  switch t := evt.(type) {
  case *disgord.MessageCreate:
    msg = t.Message
  case *disgord.MessageUpdate:
    msg = t.Message
  default:
    msg = nil
  }

  return msg
}

func messageInChannel(evt interface{}, channelPattern string) interface{} {
  var msg *disgord.Message
  if msg = getMsg(evt); msg == nil {
    return nil
  }

  var channel *disgord.Channel
  if channel = getChannel(evt); channel == nil {
    return nil
  }

  var allowedChannels = regexp.MustCompile(channelPattern)
  if allowedChannels == nil || allowedChannels.MatchString(channel.Name) {
    return nil
  }

  return evt
}

func ReplyPongToPing(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debugf("ReplyPongToPing(session, data): '%+v' '%+v'", session, data)

  var ctx context.Context
  var err error
  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()
  msg := data.Message
  channel, err := session.GetChannel(ctx, data.Message.ChannelID)


  if err != nil {
    Log.Errorf("ReplyPongToPing(Error): session.GetChannel:", err)
    return
  }
  Log.Debug("ReplyPongToPing(channel):", channel.Name)

  // whenever the message written is "ping", the bot replies "pong"
  if msg.Content != "ping" {
    return
  }

  _, err = msg.Reply(ctx, session, "pong")
  CheckErrorAndExit(err)
}
