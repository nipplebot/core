package command_handler

import (
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
  "github.com/sirupsen/logrus"
  "os"
)

const NewMessage = disgord.EvtMessageCreate
var VoiceConnections = globals.VoiceConnections

func CheckErrorAndExit(err error) {
  globals.CheckErrorAndExit(err)
}


var Log = &logrus.Logger{
  Out: os.Stderr,
  Formatter: &logrus.TextFormatter{
    //TimestampFormat: "2018-10-03 20:13:37",
    FullTimestamp: true,
  },
  Hooks: make(logrus.LevelHooks),
  Level: logrus.DebugLevel,
}
