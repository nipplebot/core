package command_handler

import (
  "context"
  "f4b.io/nipplebot/core/globals"
  "fmt"
  "github.com/andersfylling/disgord"
  "time"
)

func List(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debugf("List(session, data): '%+v' '%+v'", session, data)
  var err error

  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()

  msg := data.Message

  _, err = msg.Reply(ctx, session, fmt.Sprintf("here you go: %s", globals.SoundNamesLink))

  CheckErrorAndExit(err)
}
