package command_handler

import (
  "context"
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
  "time"
)

func Say(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debugf("Say(session, data): '%+v' '%+v'", session, data)

  var ctx context.Context
  var err error
  var replyMsg string
  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()

  msg := data.Message
  replyMsg = globals.SanitizeText(msg.Content)

  res, err := msg.Reply(ctx, session, replyMsg)
  if err != nil {
    Log.Errorf("Say(Error):", err)
    return
  }
  Log.Debugf("Said: '%v'", res)
}
