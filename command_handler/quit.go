package command_handler

import (
  "context"
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
  "os"
  "time"
)

func Quit(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debugf("Quit(session, data): '%+v' '%+v'", session, data)

  ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
  defer cancel()

  author := data.Message.Author
	channel, err := session.GetChannel(ctx, data.Message.ChannelID)
	CheckErrorAndExit(err)
	//serverID := channel.GuildID

	if author.Username == globals.AdminName {
		message := disgord.CreateMessageParams{Content: "peace out, bitches!"}
		_, err := session.CreateMessage(ctx, channel.ID, &message)
		CheckErrorAndExit(err)

		err = session.Disconnect()
		CheckErrorAndExit(err)

		err = session.Suspend()
		CheckErrorAndExit(err)

		os.Exit(0)
	}
}
