package queue

import (
  "errors"
  "f4b.io/nipplebot/core/globals"
  "github.com/nsqio/go-nsq"
)

type myMessageHandler struct{}

func processMessage(data interface{}) error {
  return errors.New("")
}

// HandleMessage implements the Handler interface.
func (h *myMessageHandler) HandleMessage(m *nsq.Message) error {
  if len(m.Body) == 0 {
    // Returning nil will automatically send a FIN command to NSQ to mark the message as processed.
    return nil
  }

  err := processMessage(m.Body)

  // Returning a non-nil error will automatically send a REQ command to NSQ to re-queue the message.
  return err
}

func StartConsumer() {
  // Instantiate a consumer that will subscribe to the provided channel.
  config := nsq.NewConfig()
  consumer, err := nsq.NewConsumer("topic", "channel", config)
  if err != nil {
    globals.Log.Fatal(err)
  }

  // Set the Handler for messages received by this Consumer. Can be called multiple times.
  // See also AddConcurrentHandlers.
  consumer.AddHandler(&myMessageHandler{})

  // Use nsqlookupd to discover nsqd instances.
  // See also ConnectToNSQD, ConnectToNSQDs, ConnectToNSQLookupds.
  err = consumer.ConnectToNSQLookupd("localhost:4161")
  if err != nil {
    globals.Log.Fatal(err)
  }

  // Gracefully stop the consumer.
  consumer.Stop()
}
