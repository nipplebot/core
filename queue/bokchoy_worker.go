package queue

import (
  "context"
  "encoding/json"
  "errors"
  "f4b.io/nipplebot/core/globals"
  "f4b.io/nipplebot/core/models"
  "github.com/andersfylling/disgord"
  "github.com/thoas/bokchoy"
  "log"
  "os"
  "os/signal"
)

func parseTask(taskPayload interface{}) (models.Command, error) {
  var command models.Command
  res, err := json.Marshal(taskPayload)

  if err != nil {
    globals.Log.Error("Marshal Error", err)
  }
  err = json.Unmarshal(res, &command)
  if err != nil {
    globals.Log.Error("Unmarshal Error", err)
  }

  return command, err
}

func StartBokchoyWorker() {
  ctx := context.Background()

  engine, err := bokchoy.New(ctx, bokchoy.Config{
    Broker: bokchoy.BrokerConfig{
      Type: "redis",
      Redis: bokchoy.RedisConfig{
        Type: "client",
        Client: bokchoy.RedisClientConfig{
          Addr: "qf4b.fritz.box:6379",
        },
      },
    },
  })
  if err != nil {
    globals.Log.Fatal(err)
  }

  engine.Queue(globals.DefaultJobQueueName).HandleFunc(
    func(r *bokchoy.Request) error {
      globals.Log.Infoln()
      globals.Log.Info("Receive request", r)

      command, err := parseTask(r.Task.Payload)
      if err != nil {
        globals.Log.Error("Error Decoding Payload: ", err)
        return err
      }
      globals.Log.Info("Decoded Command: ", command)

      switch command.Name {
      case "status":
        globals.Log.Info("> Status: ", command.Argument)
        return nil

      case "play":
        globals.Log.Info("> Playing: ", command.Argument)
        return nil

      case "connect":
        globals.Log.Info("> Connect: ", command.Argument)
        return nil

      case "ping":
        globals.Log.Info("> Ping")

        var err error
        client := globals.DiscordClient // // SetupDiscordClient(command.BotAuthToken, command.BotName)
        if client == nil {
          err := errors.New("unable to create discord client")
          globals.Log.Error("Ping: ", err.Error())
          return err
        }

        ctx, cancel := globals.DefaultMessageContext()

        defer cancel()

        //const userID = 140413331470024704
        userID := disgord.ParseSnowflakeString(command.UserId)
        currentUser, err := client.GetCurrentUser(ctx, ) // <-- bot user
        globals.Log.Info("Current User: ", userID, currentUser)
        user, err := client.GetUser(ctx, userID)
        globals.Log.Info("User: ", userID, user)

        if err != nil {
          globals.Log.Error("Ping: User: ", err.Error())
          return err
        }
        globals.Log.Info("Ping: User: ", user)

        guilds, err := client.GetCurrentUserGuilds(ctx, nil)
        if err != nil {
          globals.Log.Error("Ping: Guilds: ", err.Error())
          return err
        }
        globals.Log.Info("Ping: Guilds: ", guilds)

        connections, err := client.GetUserConnections(ctx)
        if err != nil {
          globals.Log.Error("Ping: Connections: ", err.Error())
          return err
        }
        globals.Log.Info("Ping: Connections: ", connections)

        for _, guild := range guilds {
          globals.Log.Info("Ping: Guild:", guild.Name)

          members := guild.Members
          globals.Log.Info("Ping: Guild Members:", members)

          channels := guild.Channels
          globals.Log.Info("Ping: Guild Channels:", channels)

          voiceStates := guild.VoiceStates
          globals.Log.Info("Ping: Guild VoiceStates:", voiceStates)
        }
      }
      return nil
    })

  c := make(chan os.Signal, 1)
  signal.Notify(c, os.Interrupt)

  go func() {
    for range c {
      log.Print("Received signal, gracefully stopping")
      engine.Stop(ctx)
    }
  }()

  err = engine.Run(ctx)
  if err != nil {
    log.Fatal(err)
  }
}
