module f4b.io/nipplebot/core

go 1.13.3

require (
	cloud.google.com/go v0.52.0
	emperror.dev/emperror v0.32.0
	emperror.dev/errors v0.7.0
	emperror.dev/handler/logrus v0.3.1
	github.com/andersfylling/disgord v0.16.3
	github.com/andersfylling/snowflake v1.3.0
	github.com/apache/thrift v0.12.0 // indirect
	github.com/bwmarrin/discordgo v0.20.2 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20191128021309-1d7a30a10f73 // indirect
	github.com/elazarl/goproxy v0.0.0-20191011121108-aa519ddbe484 // indirect
	github.com/elazarl/goproxy/ext v0.0.0-20191011121108-aa519ddbe484 // indirect
	github.com/enriquebris/goconcurrentqueue v0.0.0-20190719205347-3e5689c24f05
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/gomarkdown/markdown v0.0.0-20200127000047-1813ea067497
	github.com/gopherjs/gopherjs v0.0.0-20191106031601-ce3c9ade29de // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/jonas747/dca v0.0.0-20190317094138-10e959e9d3e8
	github.com/jonas747/ogg v0.0.0-20161220051205-b4f6f4cf3757 // indirect
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lib/pq v1.3.0 // indirect
	github.com/markbates/goth v1.61.0
	github.com/mattn/go-sqlite3 v2.0.2+incompatible // indirect
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/ninedraft/simplepaste v0.0.0-20151227204839-2fc55ea4861c
	github.com/nsqio/go-nsq v1.0.8
	github.com/onsi/ginkgo v1.11.0 // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/openzipkin/zipkin-go v0.1.6 // indirect
	github.com/parnurzeal/gorequest v0.2.16
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/assertions v1.0.1 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	github.com/syfaro/haste-client v0.0.0-20150731062254-09b1fbfa3977
	github.com/thoas/bokchoy v0.2.0
	github.com/thoas/go-funk v0.5.0 // indirect
	go.uber.org/atomic v1.5.1 // indirect
	go.uber.org/zap v1.13.0 // indirect
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad // indirect
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	golang.org/x/tools v0.0.0-20200128002243-345141a36859 // indirect
	google.golang.org/genproto v0.0.0-20200127141224-2548664c049f
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/ini.v1 v1.51.1 // indirect
	gopkg.in/ryankurte/go-schedule.v0 v0.0.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
