package main

import (
	"f4b.io/nipplebot/core/globals"
)

var Log = globals.Log
var VoiceConnections = globals.VoiceConnections

func CheckErrorAndExit(err error) {
	globals.CheckErrorAndExit(err)
}
