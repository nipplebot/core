package models

import (
  "github.com/andersfylling/snowflake"
  "github.com/jinzhu/gorm"
  "github.com/markbates/goth"
)

// User model
// from: https://discordapp.com/developers/docs/resources/user#user-object
//
//    id	snowflake	the user's id	identify
//    username	string	the user's username, not unique across the platform	identify
//    discriminator	string	the user's 4-digit discord-tag	identify
//    avatar	?string	the user's avatar hash	identify
//    bot?	boolean	whether the user belongs to an OAuth2 application	identify
//    mfa_enabled?	boolean	whether the user has two factor enabled on their account	identify
//    locale?	string	the user's chosen language option	identify
//    verified?	boolean	whether the email on this account has been verified	email
//    email?	string	the user's email	email
//    flags?	integer	the flags on a user's account	identify
//    premium_type?	integer	the type of Nitro subscription on a user's account	identify
type User struct {
  gorm.Model
  Id            snowflake.Snowflake
  UserName      string `json:"username"`
  Discriminator string `json:"discriminator"`
  Avatar        string `json:"avatar"`
  Bot           bool   `json:"bot,omitempty"`
  MfaEnabled    bool   `json:"mfa_enabled,omitempty"`
  Locale        string `json:"locale,omitempty"`
  Verified      bool   `json:"verified,omitempty"`
  Email         string `json:"email,omitempty"`
  Flags         int64  `json:"flags,omitempty"`
  PremiumType   int64  `json:"premium_type,omitempty"`
  RawData       map[string]interface{}
}

func ParseDiscordUser(user goth.User) (User, error) {
  var _user User
  var _err error

  // mandatory:
  _user.UserName = user.RawData["username"].(string)
  _user.Discriminator = user.RawData["discriminator"].(string)
  _user.Avatar = user.RawData["avatar"].(string)

  // optional:
  if _bot, ok := user.RawData["bot"].(bool); ok {
    _user.Bot = _bot
  }
  if _mfaEnabled, ok := user.RawData["mfa_enabled"].(bool); ok {
    _user.MfaEnabled = _mfaEnabled
  }
  if _locale, ok := user.RawData["locale"].(string); ok {
    _user.Locale = _locale
  }
  if _verified, ok := user.RawData["verified"].(bool); ok {
    _user.Verified = _verified
  }
  if _email, ok := user.RawData["email"].(string); ok {
    _user.Email = _email
  }
  if _flags, ok := user.RawData["flags"].(int64); ok {
    _user.Flags = _flags
  }
  if _premiumType, ok := user.RawData["premium_type"].(int64); ok {
    _user.PremiumType = _premiumType
  }
  if _rawData := user.RawData; _rawData != nil {
    _user.RawData = _rawData
  }
  return _user, _err
}
