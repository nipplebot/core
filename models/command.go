package models

type Command struct {
  //UserId       snowflake.Snowflake `json:"id"`
  UserId       string   `json:"id"`
  BotAuthToken string   `json:"token"`
  Name         string   `json:"name"`
  Argument     []string `json:"argument[]"`
}

func NewPingCommand(id string, token string) (p Command) {
  return Command{UserId: id, BotAuthToken: token, Name: "ping"}
}

func NewPlayCommand(argument []string) (p Command) {
  return Command{Name: "play", Argument: argument}
}

func NewStatusCommand(argument []string) (p Command) {
  return Command{Name: "status", Argument: argument}
}
