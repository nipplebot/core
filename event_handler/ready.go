package event_handler

import (
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
)

func Ready(session disgord.Session, data *disgord.VoiceState) {
  globals.Log.Debug("Ready(session, data): ", session, data)
}
