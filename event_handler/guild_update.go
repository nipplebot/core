package event_handler

import (
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
)

func GuildUpdate(session disgord.Session, data *disgord.VoiceState) {
  globals.Log.Debug("GuildUpdate(session, data): ", session, data)
}
