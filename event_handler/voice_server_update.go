package event_handler

import (
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
)

func VoiceServerUpdate(session disgord.Session, data *disgord.VoiceState) {
  globals.Log.Debug("VoiceServerUpdate(session, data): ", session, data)
}
