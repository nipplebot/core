package event_handler

import (
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
)

func UserUpdate(session disgord.Session, data *disgord.VoiceState) {
  globals.Log.Debug("UserUpdate(session, data): ", session, data)
}
