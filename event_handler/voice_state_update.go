package event_handler

import (
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
)

func VoiceStateUpdate(session disgord.Session, data *disgord.VoiceState) {
  globals.Log.Debug("VoiceStateUpdate(session, data): ", session, data)
}
