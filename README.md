# nipplebot2
Congratulations! You have successfully created a basic DisGord bot.

In order for your bot to start you must supply a environment variable with the name DISGORD_TOKEN that holds
the bot token you created in a Discord application or got from a friend.

eg. "export DISGORD_TOKEN=si7fisdgfsfushgsjdf.sdfksgjyefs.dfgysyefs"

A dockerfile has also been created for you if this is a preference. Note that you must supply the environment variable during run.


