package globals

import "github.com/andersfylling/disgord"

// DefaultBotName default path to config file
const DefaultBotName = "NippleBot"

// DefaultChannelName default channel name to config file
const DefaultChannelName = ".*"

// DefaultConfigFile default path to config file
const DefaultConfigFile = "./config.json"

// DefaultSoundsFolder default path to sounds directory
const DefaultSoundsFolder = "./sounds/"
const (
	channels  int = 2
	frameRate int = 48000
	frameSize int = 960
)

// ChannelTypeGuildText default path to config file
const ChannelTypeGuildText = iota

// ChannelTypeDM default path to config file
const ChannelTypeDM = 1

// ChannelTypeGuildVoice default path to config file
const ChannelTypeGuildVoice = 2

// ChannelTypeGroupDM default path to config file
const ChannelTypeGroupDM = 3

// ChannelTypeGuildCategory default path to config file
const ChannelTypeGuildCategory = 4

// DefaultQueueSocketNetwork default path to config file
const DefaultQueueSocketNetwork = "tcp"

// DefaultQueueSocketHost wrapper for `"tasks.job"`
const DefaultQueueSocketHost = "localhost"

// DefaultQueueSocketPort wrapper for `"tasks.job"`
const DefaultQueueSocketPort = 7419

// DefaultQueueSocketPassword wrapper for `"tasks.job"`
const DefaultQueueSocketPassword = "915c2d292943c8429829afd6ecc18e75"

// DefaultQueueSocket wrapper for `"tasks.job"`
const DefaultQueueSocket = "ipc:///tmp/queue.ipc"

// DefaultJobQueueName wrapper for `"tasks.job"`
const DefaultJobQueueName = "tasks.job"

// NewMessage wrapper for disgord.EvtMessageCreate
const NewMessage = disgord.EvtMessageCreate
