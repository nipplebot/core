package globals

import (
  "context"
  "emperror.dev/emperror"
  "fmt"
  "github.com/andersfylling/disgord"
  "os"
  "regexp"
  "strings"
  "time"

  "github.com/ninedraft/simplepaste"
  "github.com/sirupsen/logrus"
  "github.com/syfaro/haste-client"

  logrushandler "emperror.dev/handler/logrus"
)

// Log is the logger
var Log = &logrus.Logger{
  Out: os.Stderr,
  Formatter: &logrus.TextFormatter{
    //TimestampFormat: "2018-10-03 20:13:37",
    FullTimestamp: true,
  },
  Hooks: make(logrus.LevelHooks),
  Level: logrus.DebugLevel,
}

// Handler is the Handler
var Handler = emperror.WithDetails(logrushandler.New(Log))

// VoiceConnections is the Handler
var VoiceConnections = map[string]disgord.VoiceConnection{}

// AllowedAudioFileExtensions is the Handler
var AllowedAudioFileExtensions = []string{"dca", "mp3"}

// DiscordToken Discord Auth DiscordToken
var DiscordToken string

// AwsAccessKey Aws Access Key
var AwsAccessKey string

// AwsSecretKey Aws Secret Key
var AwsSecretKey string

//PastebinAPIKey Pastebin.com Api Key
var PastebinAPIKey string

// SoundsDirectory from where Sounds are being loaded
var SoundsDirectory string

// HookerName from where Sounds are being loaded
var HookerName string

// ChannelName from where Sounds are being loaded
var ChannelName string

// AdminName from where Sounds are being loaded
var AdminName string

// GoogleKey from where Sounds are being loaded
var GoogleKey string

// ConfigFile from where Sounds are being loaded
var ConfigFile string

// BotName from where Sounds are being loaded
var BotName string

// DiscordConfig from where Sounds are being loaded
var DiscordConfig disgord.Config // DiscordClient from where Sounds are being loaded

var DiscordClient *disgord.Client

// SoundNames List of possible Sound Files
var SoundNames []string

// SoundNamesLink List of possible Sound Files
var SoundNamesLink string

// SoundNames List of possible Sound Files
var errorCodesMap = map[int]string{
  0:    "error: '%s'",
  1337: "error: '%s'",
}

// AudioFile List of possible Sound Files
type AudioFile struct {
  Name   string
  Format string
}

// CheckError is a generic function to validate all errors.
func CheckError(err error, code int) {
  if err != nil {
    Log.Fatalf(errorCodesMap[code], err.Error())

    if code > 0 && code != 1337 {
      Log.Fatalln("--- exiting ---")
      os.Exit(code)
    } else {

    }
  }
}

// CheckErrorAndExit run CheckError with default parameters (code=1337, exit-if-error)
func CheckErrorAndExit(err error) {
  if err != nil {
    Handler.Handle(err)
  }
  CheckError(err, 1337)
}

// MakePasteFromList run CheckError with default parameters (code=1337, exit-if-error)
func MakePasteFromList(list []string, pastebinAPIKey string) (string, error) {
  msgBuilder := new(strings.Builder)

  for _, item := range list {
    //msgBuilder.WriteString("* ")
    msgBuilder.WriteString(item)
    msgBuilder.WriteString("\n")
  }

  api := simplepaste.NewAPI(pastebinAPIKey)

  paste := simplepaste.NewPaste("nipplebot sounds", msgBuilder.String())
  paste.ExpireDate = simplepaste.Day //your paste will be available for one month
  return api.SendPaste(paste)        //returns link to the paste and nil, if everything is ok
}

// MakeHasteFromStringList run CheckError with default parameters (code=1337, exit-if-error)
func MakeHasteFromStringList(list []string) (url *string, ok bool) {
  msgBuilder := new(strings.Builder)

  for _, item := range list {
    //msgBuilder.WriteString("* ")
    msgBuilder.WriteString(item)
    msgBuilder.WriteString("\n")
  }

  hasteClient := haste.NewHaste("https://hastebin.com")
  resp, err := hasteClient.UploadString(msgBuilder.String())

  if resp != nil && err == nil {
    Log.Debugf("MakeHasteFromStringList(resp): '%s'", resp)
    url := resp.GetLink(hasteClient) + ".md"
    return &url, true
  }

  Log.Errorf("MakeHasteFromStringList(error): '%v'", err)
  return nil, false
}

// DefaultQueueSocketAddress run CheckError with default parameters (code=1337, exit-if-error)
func DefaultMessageContext() (context.Context, context.CancelFunc) {
  return context.WithDeadline(context.Background(), time.Now().Add(1*time.Minute))
}

// DefaultQueueSocketAddress run CheckError with default parameters (code=1337, exit-if-error)
func DefaultQueueSocketAddress() string {
  return fmt.Sprintf("%s:%d", DefaultQueueSocketHost, DefaultQueueSocketPort)
}

// SanitizeText
func SanitizeText(inString string) string {
  // Make a Regex to say we only want letters and numbers
  reg, err := regexp.Compile("[^a-zA-Z0-9]+")
  if err != nil {
    Log.Errorf("SanitizeText(error): '%v'", err)
    return err.Error()
  }
  processedString := reg.ReplaceAllString(inString, "")
  return fmt.Sprintf("%s", processedString)
}
