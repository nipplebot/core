package cmd

import (
  "f4b.io/nipplebot/core/globals"
  "io/ioutil"
  "path/filepath"
  "strings"
)

func loadSounds() {
  var soundsDirectory = globals.DefaultSoundsFolder

  if len(globals.SoundsDirectory) > 0 {
    soundsDirectory = globals.SoundsDirectory
  }
  absSoundsDirectory, err := filepath.Abs(soundsDirectory)
  globals.CheckErrorAndExit(err)

  soundFiles, err := ioutil.ReadDir(absSoundsDirectory)

  for _, f := range soundFiles {
    for _, ext := range globals.AllowedAudioFileExtensions {
      if !f.IsDir() && strings.HasSuffix(f.Name(), "."+ext) {
        audioFileName := strings.TrimSuffix(f.Name(), "."+ext)
        globals.Log.Debugf("Found Audio File: '%s'", audioFileName)
        // SoundNames = append(SoundNames, AudioFile{Name: audioFileName, Format: ext})
        globals.SoundNames = append(globals.SoundNames, audioFileName)
      }
    }
  }
  globals.Log.Debugf("#'%d' Audio Files loaded!", len(globals.SoundNames))

  globals.SoundNamesLink, err = globals.MakePasteFromList(globals.SoundNames, globals.PastebinAPIKey)
  globals.CheckErrorAndExit(err)
}

func Init() {
  globals.Log.Debug("init()")

  loadSounds()
  // ...
}
