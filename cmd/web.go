package cmd

import (
  "fmt"
  "html/template"
  "net/http"
  "path/filepath"

  "f4b.io/nipplebot/core/globals"
  "github.com/gomarkdown/markdown"
  "github.com/gomarkdown/markdown/html"
  "github.com/microcosm-cc/bluemonday"
)

var httpHost = "127.0.0.1"
var httpPort = 19000
var outputHtml = ""

func serveTemplate(w http.ResponseWriter, r *http.Request) {
	lp := filepath.Join("templates", "layout.html")
	fp := filepath.Join("templates", filepath.Clean(r.URL.Path))

	tmpl, _ := template.ParseFiles(lp, fp)
	err := tmpl.ExecuteTemplate(w, "layout", struct {
		List string
	}{
		List: outputHtml,
	})
	globals.CheckError(err, 4)
}

func webHandler(w http.ResponseWriter, r *http.Request) {
	code, err := fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
  globals.CheckError(err, 3)

	Log.Infof("Web server handled request, code: %d", code)
}

func StartWebServer(md string) {
	htmlFlags := html.CommonFlags | html.HrefTargetBlank
	opts := html.RendererOptions{Flags: htmlFlags}
	renderer := html.NewRenderer(opts)
	unsafeHtml := markdown.ToHTML([]byte(md), nil, renderer)

	outputHtml = string(bluemonday.UGCPolicy().SanitizeBytes(unsafeHtml))

	http.HandleFunc("/debug", webHandler)
	http.HandleFunc("/", serveTemplate)

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", fs)

	Log.Infof("Web server running at '%s:%d'", httpHost, httpPort)
	err := http.ListenAndServe(fmt.Sprintf("%s:%d", httpHost, httpPort), nil)
	globals.CheckError(err, 3)
}
