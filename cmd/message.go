package cmd

import (
  "github.com/andersfylling/disgord"
)

func onMessageCreate(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debug("onMessageCreate(session):", session)
  Log.Debug("onMessageCreate(data):", data)

  // Ignore all messages created by the bot itself
  // This isn't required in this specific example but it'session a good practice.
  //if data.Author.ID == session.GetCurrentUser() {
  //  return
  //}
  //if len(data.Content) == 0 {
  //  return
  //}
  //if len(data.Embeds) > 0 {
  //  return
  //}
  //if data.Content[:1] != "!" {
  //  return
  //}

  //channel, err := session.Channel(data.ChannelID)
  //globals.CheckErrorAndExit(err)
  //
  //author := data.Author
  //message := data.Content
  //match, _ := regexp.MatchString(globals.ChannelName, channel.Name)
  //
  //if !match {
  //  return
  //}
  //
  //Log.Debugf("[%s] > %20s\n", author.String(), message)
  //
  //serverID := disgord.NewSnowflake(0) // channel.GuildID
  //args := strings.Split(message, " ")
  //command := args[0][1:]
  //commandArgs := []string{""}

  //if len(args) > 1 {
  //  commandArgs = args[1:]
  //}

  // --- --------------
  // VOICE COMMANDS
  // ---  --------------
  //if funk.Contains(globals.SoundNames, command) { // || funk.Contains(globals.VoiceCommandNames, command) {
  //  if VoiceInstances[serverID] == nil {
  //    var msg strings.Builder
  //    author.Mention()
  //    msg.WriteString("Hey " + author.Mention() + ", I'm not in any voice channel atm...")
  //    msg.WriteString("\n")
  //    msg.WriteString("Do you want me to join? (answer with `!yes`, use `!come` command or thumbs-up this message)")
  //
  //    _, err := session.ChannelMessageSend(channel.ID, msg.String())
  //    globals.CheckErrorAndExit(err)
  //    return
  //  }
  //
  //  if command == "play" {
  //    // TODO: this
  //    return
  //  }
  //  if command == "stop" {
  //    VoiceInstances[serverID].StopAudio()
  //    return
  //  }
  //  if command == "skip" {
  //    VoiceInstances[serverID].SkipAudio()
  //    return
  //  }
  //
  //  if command == "say" {
  //    textToSpeak := strings.Join(args[1:], " ")
  //    speechFile, err := textToMp3speech(textToSpeak)
  //    globals.CheckErrorAndExit(err)
  //
  //    var ext = filepath.Ext(speechFile.Name())
  //    var dir = filepath.Dir(speechFile.Name())
  //    var name = filepath.Base(speechFile.Name()[0:(len(speechFile.Name()) - len(ext))])
  //
  //    Log.Debugf("speechFile: '%s' '%s' '%s'", ext, dir, name)
  //
  //    VoiceInstances[serverID].QueueAudio(dir, name)
  //    return
  //  }
  //
  //  VoiceInstances[serverID].QueueAudio(globals.SoundsDirectory, command)
  //  globals.CheckErrorAndExit(err)
  //  return
  //}

  // ---  --------------
  // TEXT COMMANDS
  // ---  --------------

  // !next
  //if command == "next" {
  //  //command_handler.Next(session, event, commandArgs)
  //  return
  //}
  //// !list
  //if command == "list" {
  //  //command_handler.List(session, event)
  //  return
  //}
  //// !come
  //if command == "come" {
  //  //command_handler.Come(session, event, commandArgs)
  //  return
  //}
  //// !leave
  //if command == "leave" {
  //  //command_handler.Leave(session, event, commandArgs)
  //  return
  //}
  //// !quit
  //if command == "quit" {
  //  //command_handler.Quit(session, event, commandArgs)
  //  return
  //}
  //// !help
  //if command == "help" {
  //  var msg strings.Builder
  //
  //  msg.WriteString("**!play** <url> - play youtube video; queues up if another video is playing")
  //  msg.WriteString("\n")
  //  msg.WriteString("**!skip** - skip currently playing video")
  //  msg.WriteString("\n")
  //  msg.WriteString("**!stop** - stops Video and clears queue")
  //  msg.WriteString("\n")
  //  msg.WriteString("**!come [voice]** - join voice channel")
  //  msg.WriteString("\n")
  //  msg.WriteString("**!leave [voice]** - leave voice channel")
  //  msg.WriteString("\n")
  //  msg.WriteString("**!list** - get List of Sound Names")
  //  msg.WriteString("\n")
  //  msg.WriteString("**!<sound name>** - say <sound name>")
  //
  //  _, err := session.ChannelMessageSend(data.ChannelID, msg.String())
  //  globals.CheckErrorAndExit(err)
  //  return
  //}
}
