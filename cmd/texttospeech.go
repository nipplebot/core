package cmd

import (
  "context"
  "f4b.io/nipplebot/core/globals"
  "github.com/jonas747/dca"
  "io/ioutil"
  "os"
  "path/filepath"

  texttospeech "cloud.google.com/go/texttospeech/apiv1"
  texttospeechpb "google.golang.org/genproto/googleapis/cloud/texttospeech/v1"
)

func textToMp3speech(text string) (*os.File, error) {
	tmpSpeechDir, err := ioutil.TempDir(globals.SoundsDirectory, "speech")
	tmpMp3File, err := ioutil.TempFile(tmpSpeechDir, "speech.*.mp3")

	// Instantiates a client.
	ctx := context.Background()

	client, err := texttospeech.NewClient(ctx)
	if err != nil {
		Log.Fatal(err)
	}

	// Perform the text-to-speech request on the text input with the selected
	// voice parameters and audio file type.
	req := texttospeechpb.SynthesizeSpeechRequest{
		// Set the text input to be synthesized.
		Input: &texttospeechpb.SynthesisInput{
			InputSource: &texttospeechpb.SynthesisInput_Text{Text: text},
		},
		// Build the voice request, select the language code ("en-US") and the SSML
		// voice gender ("neutral").
		Voice: &texttospeechpb.VoiceSelectionParams{
			LanguageCode: "en-US",
			SsmlGender:   texttospeechpb.SsmlVoiceGender_NEUTRAL,
		},
		// Select the type of audio file you want returned.
		AudioConfig: &texttospeechpb.AudioConfig{
			AudioEncoding: texttospeechpb.AudioEncoding_MP3,
		},
	}

	resp, err := client.SynthesizeSpeech(ctx, &req)
	CheckErrorAndExit(err)

	encodeSession, err := dca.EncodeFile(tmpMp3File.Name(), dca.StdEncodeOptions)
	globals.CheckError(err, 0)

	defer encodeSession.Cleanup()

	tmpFileDca, err := ioutil.TempFile(tmpSpeechDir, "speech.*.dca")
  globals.CheckError(err, 0)

	err = ioutil.WriteFile(tmpFileDca.Name(), resp.AudioContent, 0644)
  globals.CheckError(err, 0)

	var ext = filepath.Ext(tmpFileDca.Name())
	var dir = filepath.Dir(tmpFileDca.Name())
	var name = filepath.Base(tmpFileDca.Name()[0:(len(tmpFileDca.Name()) - len(ext))])

	Log.Debugf("'%s' '%s' '%s'", ext, dir, name)

	return tmpFileDca, nil
}
