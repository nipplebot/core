package cmd

import (
  "f4b.io/nipplebot/core/globals"
  "fmt"
  "github.com/andersfylling/disgord"
  "os"
  "time"

  Snowflake "github.com/andersfylling/snowflake"
  "github.com/enriquebris/goconcurrentqueue"
  _ "github.com/jonas747/dca"
)

// VoiceInstance is created for each connected server
type VoiceInstance struct {
  discord      disgord.Session
  queue        *goconcurrentqueue.FIFO
  voice        disgord.VoiceConnection
  pcmChannel   chan []int16
  channelID    disgord.Snowflake
  serverID     disgord.Snowflake
  skip         bool
  stop         bool
  trackPlaying bool
}

var VoiceInstances = map[disgord.Snowflake]*VoiceInstance{}

func onVoiceStateUpdate(session disgord.Session, event *disgord.VoiceStateUpdate) {
  Log.Debug("onVoiceStateUpdate(event):", event.ChannelID, event.GuildID, event.UserID, event.VoiceState)
  return
}

func (vi *VoiceInstance) playDcaAudio(fileName string) {
  Log.Debugf("Playing dca audio file '%s'", fileName)
  vi.trackPlaying = true

  err := vi.discord.UpdateStatusString(fileName)
  CheckErrorAndExit(err)

  err = vi.voice.StartSpeaking()
  globals.CheckErrorAndExit(err)

  time.Sleep(1 * time.Second)

  file, err := os.Open(fmt.Sprintf("%s.dca", fileName))
  err = vi.voice.SendDCA(file)
  globals.CheckErrorAndExit(err)

  time.Sleep(1 * time.Second)

  err = vi.voice.StopSpeaking()
  globals.CheckErrorAndExit(err)

  err = vi.discord.UpdateStatusString("!help")
  globals.CheckErrorAndExit(err)

  //file, err := os.Open(fmt.Sprintf("%s.dca", fileName))
  //globals.CheckError(err, 0)
  //
  //time.Sleep(1 * time.Second)
  //
  //// inputReader is an io.Reader, like a file for example
  //decoder := dca.NewDecoder(file)
  //for {
  //  frame, err := decoder.OpusFrame()
  //  if err != nil && err == io.EOF {
  //    break
  //  }
  //  globals.CheckError(err, 0)
  //  vi.voice.OpusSend <- frame
  //}
  //
  //time.Sleep(1 * time.Second)
  //
  //err = vi.discord.UpdateStatus(0, "!help")
  //globals.CheckErrorAndExit(err)
}

// StopAudio marks to stop all tracks and clears queue on the next binary read.
func (vi *VoiceInstance) StopAudio() {
  vi.stop = true
}

// SkipAudio skips the current playing track
func (vi *VoiceInstance) SkipAudio() {
  vi.skip = true
}

func (vi *VoiceInstance) JoinVoiceChannel() {
  dgv, err := vi.discord.VoiceConnect(vi.serverID, vi.channelID)
  globals.CheckErrorAndExit(err)

  vi.voice = dgv
}

func (vi *VoiceInstance) LeaveVoiceChannel() {
  err := vi.voice.Close()
  globals.CheckErrorAndExit(err)
}

// QueueVideo places a Youtube link in a queue
func (vi *VoiceInstance) QueueVideo(url string) {
  Log.Debugf("Queuing video '%s'", url)
  err := vi.queue.Enqueue(fmt.Sprintf("%s", url))
  globals.CheckErrorAndExit(err)
}

// QueueAudio places a Youtube link in a queue
func (vi *VoiceInstance) QueueAudio(filePath string, fileName string) {
  Log.Debugf("Queuing audio file '%s' in directory '%s'", fileName, filePath)
  err := vi.queue.Enqueue(fmt.Sprintf("%s/%s", filePath, fileName))
  globals.CheckErrorAndExit(err)
}

// CreateVoiceInstance accepts both a youtube query and a server id, boots up the voice connection, and plays the track.
func CreateVoiceInstance(serverID uint64, channelID uint64) {
  Log.Infof("Creating Voice Instance at server '%s'", serverID)
  done := make(chan struct{})
  vi := new(VoiceInstance)

  vi.serverID = disgord.Snowflake(Snowflake.NewSnowflake(serverID))
  vi.channelID = disgord.Snowflake(Snowflake.NewSnowflake(channelID))
  vi.queue = goconcurrentqueue.NewFIFO()
  VoiceInstances[vi.serverID] = vi

  Log.Info("Connecting Voice...")

  err := vi.queue.Enqueue("QUEUE TEST: Queue ready!")
  globals.CheckErrorAndExit(err)
  value, err := vi.queue.Dequeue()
  globals.CheckErrorAndExit(err)
  Log.Infof(fmt.Sprintf("%+v", value))

  go func() {
    value, err := vi.queue.DequeueOrWaitForNextElement()
    globals.CheckErrorAndExit(err)

    if value != nil {
      vi.playDcaAudio(fmt.Sprintf("%+v", value))
    }
    done <- struct{}{}
  }()
  <-done
}
