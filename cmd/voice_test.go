package cmd

import (
	"github.com/andersfylling/disgord"
	"github.com/enriquebris/goconcurrentqueue"
	_ "github.com/jonas747/dca"
	"testing"
)

func TestVoiceInstance_playVideo(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	type args struct {
		url string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.QueueVideo(tt.args.url)
		})
	}
}

func TestVoiceInstance_StopVideo(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.StopVideo()
		})
	}
}

func TestVoiceInstance_SkipVideo(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.SkipVideo()
		})
	}
}

func TestVoiceInstance_playAudio(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	type args struct {
		fileName string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.playDcaAudio(tt.args.fileName)
		})
	}
}

func TestVoiceInstance_StopAudio(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.StopAudio()
		})
	}
}

func TestVoiceInstance_SkipAudio(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.SkipAudio()
		})
	}
}

func TestVoiceInstance_connectVoice(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.QueueAudio("folder", "file.dca")
		})
	}
}

func TestVoiceInstance_QueueVideo(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	type args struct {
		url string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.QueueVideo(tt.args.url)
		})
	}
}

func TestVoiceInstance_QueueAudio(t *testing.T) {
	type fields struct {
		discord      disgord.Session
		queue        *goconcurrentqueue.FIFO
		voice        disgord.VoiceConnection
		pcmChannel   chan []int16
		channelID    disgord.Snowflake
		serverID     disgord.Snowflake
		skip         bool
		stop         bool
		trackPlaying bool
	}
	type args struct {
		fileDir  string
		fileName string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			vi := &VoiceInstance{
				discord:      tt.fields.discord,
				queue:        tt.fields.queue,
				pcmChannel:   tt.fields.pcmChannel,
				voice:        tt.fields.voice,
				serverID:     tt.fields.serverID,
				skip:         tt.fields.skip,
				stop:         tt.fields.stop,
				trackPlaying: tt.fields.trackPlaying,
			}
			vi.QueueAudio(tt.args.fileDir, tt.args.fileName)
		})
	}
}

func TestCreateVoiceInstance(t *testing.T) {
	type args struct {
		serverID  uint64
		channelID uint64
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			CreateVoiceInstance(tt.args.serverID, tt.args.channelID)
		})
	}
}
