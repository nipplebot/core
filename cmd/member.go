package cmd

import (
  "github.com/andersfylling/disgord"
)

func onGuildMemberUpdate(session disgord.Session, event *disgord.GuildMemberUpdate) {
	Log.Debug("onGuildMemberUpdate(event):", event)

}
func onGuildMembersChunk(session disgord.Session, event *disgord.GuildMembersChunk) {
	Log.Debug("onGuildMembersChunk(event):", event)

	for _, m := range event.Members {
		Log.Debug("onGuildMembersChunk(member):", m.User.String())
	}
}
