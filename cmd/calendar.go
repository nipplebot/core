package cmd

import (
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
  "gopkg.in/ryankurte/go-schedule.v0"
  "time"
)

var calendarInstances = map[string]*CalendarInstance{}

func init() {

}

// VoiceInstance is created for each connected server
type CalendarInstance struct {
	discord   *disgord.Session
	scheduler scheduler.Scheduler
}

// CreateEvent is a generic function to create an event
func (ci *CalendarInstance) CreateEvent() {
	name := ""
	description := ""
	when := time.Now()
	repeat := scheduler.RepeatNever

	_, err := ci.scheduler.Schedule(name, description, when, repeat)
	globals.CheckErrorAndExit(err)
}

func CreateCalendarInstance(serverID string) {
  globals.Log.Info("Creating Calendar Instance")

	ci := new(CalendarInstance)

	calendarInstances[serverID] = ci
}
