package cmd

import (
  "f4b.io/nipplebot/core/command_handler"
  "f4b.io/nipplebot/core/globals"
  "github.com/andersfylling/disgord"
  "github.com/andersfylling/disgord/std"
)

// CheckError is a generic function to validate all errors.
func SetupDiscordClient(token string, name string) *disgord.Client {
  var discordConfig disgord.Config
  var discordClient *disgord.Client

  ctx, cancel := globals.DefaultMessageContext()
  defer cancel()

  discordConfig = disgord.Config{
    BotToken:    token,
    Logger:      globals.Log,
    ProjectName: name,
  }
  globals.DiscordConfig = discordConfig

  discordClient = disgord.New(discordConfig)

  log, _ := std.NewLogFilter(discordClient)
  filter, _ := std.NewMsgFilter(ctx, discordClient)
  filter.SetPrefix("!")
  filter.SetAltPermissions()

  pingFilter, _ := std.NewMsgFilter(ctx, discordClient)
  pingFilter.SetPrefix("ping")

  comeFilter, _ := std.NewMsgFilter(ctx, discordClient)
  comeFilter.SetPrefix("come")

  listFilter, _ := std.NewMsgFilter(ctx, discordClient)
  listFilter.SetPrefix("list")

  sayFilter, _ := std.NewMsgFilter(ctx, discordClient)
  sayFilter.SetPrefix("say")

  playFilter, _ := std.NewMsgFilter(ctx, discordClient)
  playFilter.SetPrefix("play")

  quitFilter, _ := std.NewMsgFilter(ctx, discordClient)
  quitFilter.SetPrefix("quit")

  //discordClient.On(disgord.EvtReady,
  //  //std.CopyMsgEvt, // read & copy original
  //  // event log
  //  //log.LogMsg,
  //  // handler
  //  event_handler.Ready, // handles voice state
  //)

  //discordClient.On(disgord.EvtUserUpdate,
  //  // handler
  //  event_handler.UserUpdate, // handles voice state
  //)
  //
  //discordClient.On(disgord.EvtChannelUpdate,
  //  // handler
  //  event_handler.ChannelUpdate, // handles voice state
  //)
  //
  //discordClient.On(disgord.EvtVoiceServerUpdate,
  //  // handler
  //  event_handler.VoiceServerUpdate, // handles voice state
  //)
  //
  //discordClient.On(disgord.EvtVoiceStateUpdate,
  //  // handler
  //  event_handler.VoiceStateUpdate, // handles voice state
  //)
  //
  //discordClient.On(disgord.EvtGuildUpdate,
  //  std.CopyMsgEvt, // read & copy original
  //  // event log
  //  log.LogMsg,
  //  // handler
  //  event_handler.VoiceStateUpdate, // handles voice state
  //)

  // create a handler and bind it to new message events
  // tip: read the documentation for std.CopyMsgEvt and understand why it is used here.
  discordClient.On(disgord.EvtMessageCreate,
    // middleware
    filter.NotByBot,    // ignore bot messages
    filter.HasPrefix,   // read original
    std.CopyMsgEvt,     // read & copy original
    filter.StripPrefix, // write copy
    // command filter
    pingFilter.HasPrefix,
    // command log
    log.LogMsg,
    // handler
    command_handler.ReplyPongToPing, // handles ping pong
  )

  discordClient.On(disgord.EvtMessageCreate,
    // middleware
    filter.NotByBot,    // ignore bot messages
    filter.HasPrefix,   // read original
    std.CopyMsgEvt,     // read & copy original
    filter.StripPrefix, // write copy
    // command filter
    comeFilter.HasPrefix,
    // command log
    log.LogMsg,
    // handler
    command_handler.Come, // handles come command
  )

  discordClient.On(disgord.EvtMessageCreate,
    // middleware
    filter.NotByBot,    // ignore bot messages
    filter.HasPrefix,   // read original
    std.CopyMsgEvt,     // read & copy original
    filter.StripPrefix, // write copy
    // command filter
    sayFilter.HasPrefix,
    sayFilter.StripPrefix,
    // command log
    log.LogMsg,
    // handler
    command_handler.Say, // handles say command
  )

  discordClient.On(disgord.EvtMessageCreate,
    // middleware
    filter.NotByBot,    // ignore bot messages
    filter.HasPrefix,   // read original
    std.CopyMsgEvt,     // read & copy original
    filter.StripPrefix, // write copy
    // command filter
    playFilter.HasPrefix,
    playFilter.StripPrefix,
    // command log
    log.LogMsg,
    // handler
    command_handler.PlayAudio, // handles play command
  )

  discordClient.On(disgord.EvtMessageCreate,
    // middleware
    filter.NotByBot,    // ignore bot messages
    filter.HasPrefix,   // read original
    std.CopyMsgEvt,     // read & copy original
    filter.StripPrefix, // write copy
    // command filter
    quitFilter.HasPrefix,
    quitFilter.StripPrefix,
    // command log
    log.LogMsg,
    // handler
    command_handler.Quit, // handles quit command
  )

  return discordClient
}
