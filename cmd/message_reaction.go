package cmd

import (
  "github.com/andersfylling/disgord"
  "time"
)

func onMessageReactionAdd(session disgord.Session, data *disgord.MessageCreate) {
  Log.Debug("onMessageReactionAdd(session):", time.Now().Format(time.Stamp), session)
  Log.Debug("onMessageReactionAdd(data):", time.Now().Format(time.Stamp), data)
}
