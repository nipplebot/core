package cmd

import (
	"encoding/binary"
	"fmt"
	"io"
	"net/http"
	"os/exec"
	"strconv"
)

var frameRate int
var frameSize int
var channels int
var run *exec.Cmd

func (vi *VoiceInstance) playVideo(url string) {
	vi.trackPlaying = true

	resp, err := http.Get(url)
	if err != nil {
		Log.Errorf("Http.Get\nerror: %s\ntarget: %s\n", err, url)
		return
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			Log.Errorf("resp.Body.Close\nerror: %s\ntarget: %s\n", err, url)
			return
		}
	}()

	if resp.StatusCode != 200 {
		Log.Printf("reading answer: non 200 status code received: '%s'", err)
	}

	run = exec.Command("ffmpeg", "-i", "-", "-f", "s16le", "-ar",
		strconv.Itoa(frameRate), "-ac", strconv.Itoa(channels), "pipe:1")
	run.Stdin = resp.Body
	stdout, err := run.StdoutPipe()
	if err != nil {
		fmt.Println("StdoutPipe Error:", err)
		return
	}

	err = run.Start()
	if err != nil {
		fmt.Println("RunStart Error:", err)
		return
	}

	// buffer used during loop below
	audioBuff := make([]int16, frameSize*channels)

	//vi.discord.Voice.Speaking(true)
	//defer vi.discord.Voice.Speaking(false)

	for {
		// read data from ffmpeg stdout
		err = binary.Read(stdout, binary.LittleEndian, &audioBuff)
		if err == io.EOF || err == io.ErrUnexpectedEOF {
			break
		}
		if err != nil {
			fmt.Println("error reading from ffmpeg stdout :", err)
			break
		}
		if vi.stop == true || vi.skip == true {
			err = run.Process.Kill()
			break
		}
		vi.pcmChannel <- audioBuff
	}

	vi.trackPlaying = false
}

// StopVideo marks to stop all tracks and clears queue on the next binary read.
func (vi *VoiceInstance) StopVideo() {
	vi.stop = true
}

// SkipVideo skips the current playing track
func (vi *VoiceInstance) SkipVideo() {
	vi.skip = true
}
