package cmd

import (
  "github.com/andersfylling/disgord"
  "f4b.io/nipplebot/core/globals"
)

const NewMessage = disgord.EvtMessageCreate
var Log = globals.Log

func CheckErrorAndExit(err error) {
  globals.CheckErrorAndExit(err)
}
